#!/bin/bash -x

DOCKER_VERSION="20.10.12"
ALPINE_VERSION="3.15"
BUILDX_VERSION="0.7.1"

REGISTRY="registry.gitlab.com/calfox/tools/docker-buildx"
TAG="${DOCKER_VERSION}_${BUILDX_VERSION}"

PLATFORMS=(
  linux/amd64
  linux/arm64
)

for PLATFORM in ${PLATFORMS[@]}; do
  BUILDX_ARCH=${PLATFORM//\//-}

  mkdir -p "bin/${PLATFORM}"
  wget -nc -O "bin/${PLATFORM}/buildx-v${BUILDX_VERSION}" "https://github.com/docker/buildx/releases/download/v${BUILDX_VERSION}/buildx-v${BUILDX_VERSION}.${BUILDX_ARCH}"
done

docker buildx build \
  -t ${REGISTRY}:${TAG} \
  --platform "$(echo ${PLATFORMS[@]} | sed 's/ /,/g')" \
  --build-arg DOCKER_VERSION=${DOCKER_VERSION} \
  --build-arg ALPINE_VERSION=${ALPINE_VERSION} \
  --build-arg BUILDX_VERSION=${BUILDX_VERSION} \
  --push \
  .
